const namespaced = true

const state = {
  formData: {}
}

const mutations = {
  set(state, data) {
    state.formData = { ...data }
  }
}

const actions = {
  setFormData({ commit }, data) {
    commit('set', data)
  }
}

export default {
  namespaced,
  state,
  mutations,
  actions
}
